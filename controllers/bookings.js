const express =require('express');
const Booking =require('../models/booking');

function list(req, res, next) {
    let page = req.params.page ? req.params.page : 1;
    Booking.paginate({}, {page:page, limit:3}).then(objs => res.status(200).json({
       //message: "Lista de reservas del sistema",
       "message": res.__('list.bookings'),
       obj: objs
    })).catch(ex => res.status(500).json({
       //message: "No se pudo consultar la info de las reservaciones",
       "message": res.__('list.nobookings'),
       obj: ex
    }));
}

function index(req,res,next){   
   const id =req.params.id;
   Booking.findOne({"_id":id}).populate('_member _copy').then(obj => res.status(200).json({
       //message: `Booking almacenado con ID ${id}`,
       "message": res.__('index.bookings'),
       obj: obj
   })).catch(ex => res.status(500).json({
        //message: `No se pudo consultar la info del Booking con id ${id}`,
        "message": res.__('index.nobookings'),
        obj: ex
    }));
}

function create(req,res,next){
    const copy = req.body.copy;//implicitos o sobre el cuerpo
    const member = req.body.member;
    const date = req.body.member;
   
    let booking = new Booking({
        copy:copy,
        member:member,
        date:date
    });

    booking.save().then(obj => res.status(200).json({
        //message : 'reservación creada correctamente',
        "message": res.__('create.bookings'),
        obj: obj
    })).catch(ex =>res.status(500).json({
        //message: 'No se pudo almacena la reservación',
        "message": res.__('create.nobookings'),
        obj: ex
    }));
}

function replace(req,res,next){
    const id = req.params.id;
    let copy = req.body.copy ? req.body.copy: "";
    let member = req.body.member ? req.body.member: "";
    let date = req.body.date ? req.body.date: "";

    let booking = new Object({
        _copy: copy,
        _member : member,
        _date : date
    });

    Booking.findOneAndUpdate({"_id":id},booking).then(obj => res.status(200).json({
        //message: "Reservación remplazado correctamente",
        "message": res.__('replace.bookings'),
        obj:obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo remplazar la reservación con id ${id}`,
        "message": res.__('replace.nobookings'),
        obj: ex
    }));

}

function edit(req,res,next){
    const id = req.params.id;
    const copy = req.body.copy;//implicitos o sobre el cuerpo
    const member = req.body.member;
    const date = req.body.member;

    let booking = new Object();

    if(copy){
        booking._copy = copy;
    }

    if(member){
        booking._member = member;
    }

    if(date){
        booking._date = date;
    }

    Booking.findOneAndUpdate({"_id":id},booking).then(obj => res.status(200).json({
        //message: "Booking Actualizado correctamente",
        "message": res.__('edit.bookings'),
        obj:obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo actualizar el booking con id ${id}`,
        "message": res.__('edit.nobookings'),
        obj: ex
    }));
}

function destroy(req,res,next){
    const id = req.params.id;
    Booking.remove({"_id":id}).then(obj => res.status(200).json({
        //message: "Booking eliminado correctamente",
        "message": res.__('destroy.bookings'),
        obj:obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo eliminar el Booking con id ${id}`,
        "message": res.__('destroy.nobookings'),
        obj: ex
    }));
}

module.exports ={
    list, index,create,edit,replace,destroy
}