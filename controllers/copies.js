const express =require('express');
const Copy =require('../models/copy');

function list(req, res, next) {
    let page = req.params.page ? req.params.page : 1;
    Copy.paginate({}, {page:page, limit:3}).then(objs => res.status(200).json({
       //message: "Lista de copias del sistema",
       "message": res.__('list.copies'),
       obj: objs
    })).catch(ex => res.status(500).json({
       //message: "No se pudo consultar la info de las copia",
       "message": res.__('list.nocopies'),
       obj: ex
    }));
}

function index(req,res,next){   
   const id =req.params.id;
   Copy.findOne({"_id":id}).then(obj => res.status(200).json({
       //message: `Copia almacenado con ID ${id}`,
       "message": res.__('index.copies'),
       obj: obj
   })).catch(ex => res.status(500).json({
        //message: `No se pudo consultar la info de la copia con id ${id}`,
        "message": res.__('index.nocopies'),
        obj: ex
    }));
}

function create(req,res,next){
    const format = req.body.format;//implicitos o sobre el cuerpo
    const movie = req.body.movie;
    const number = req.body.number;
    const status = req.body.status;
   
    let copy = new Copy({
        format:format,
        movie:movie,
        number:number,
        status:status
    });

    copy.save().then(obj => res.status(200).json({
        //message : 'copia creada correctamente',
        "message": res.__('create.copies'),
        obj: obj
    })).catch(ex =>res.status(500).json({
        //message: 'No se pudo almacena la copia',
        "message": res.__('create.nocopies'),
        obj: ex
    }));
}

function replace(req,res,next){
    const id = req.params.id;
    let format = req.body.format ? req.body.format: "";
    let movie = req.body.movie ? req.body.movie: "";
    let number = req.body.number ? req.body.number: "";
    let status = req.body.status ? req.body.status:"";

    let copy = new Object({
        _format:format,
        _movie:movie,
        _number:number,
        _status:status
    });

    Copy.findOneAndUpdate({"_id":id},copy).then(obj => res.status(200).json({
        //message: "Copia remplazada correctamente",
        "message": res.__('replace.copies'),
        obj:obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo remplazar la Copia con id ${id}`,
        "message": res.__('replace.nocopies'),
        obj: ex
    }));

}

function edit(req,res,next){
    const id = req.params.id;
    const format = req.body.format;//implicitos o sobre el cuerpo
    const movie = req.body.movie;
    const number = req.body.number;
    const status = req.body.status;

    let copy = new Object();

    if(format){
        copy._format = format;
    }

    if(movie){
        copy._movie = movie;
    }

    if(number){
        copy._number = number;
    }

    if(status){
        copy._status = status;
    }

    Copy.findOneAndUpdate({"_id":id},copy).then(obj => res.status(200).json({
        //message: "Copia Actualizada correctamente",
        "message": res.__('edit.copies'),
        obj:obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo actualizar la copia con id ${id}`,
        "message": res.__('edit.nocopies'),
        obj: ex
    }));
}

function destroy(req,res,next){
    const id = req.params.id;
    Copy.remove({"_id":id}).then(obj => res.status(200).json({
        //message: "Copia eliminado correctamente",
        "message": res.__('destroy.copies'),
        obj:obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo eliminar el copia con id ${id}`,
        "message": res.__('destroy.nocopies'),
        obj: ex
    }));
}

module.exports ={
    list, index,create,edit,replace,destroy
}