const express = require('express');
const Permission = require('../models/permission');
const acl = require('acl');


function list(req, res, next) {
    Permission.find().then(objs => res.status(200).json({
        //message: "Lista de permisos del sistema",
        "message": res.__('list.permisos'),
        obj: objs
    })).catch(ex => res.status(500).json({
        //message: "No se pudo consultar la informacion de los permisos",
        "message": res.__('list.nopermisos'),
        obj: ex
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Permission.findOne({"_id":id}).then(obj => res.status(200).json({
        //message: `Permiso almacenando con ID: ${id}`,
        "message": res.__('index.permisos'),
        obj: obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo consultar la informacion del permiso con ID: ${id}`,
        "message": res.__('index.nopermisos'),
        obj: ex
    }));
}

function create(req, res, next){
    const description = req.body.description;//implicitos o sobre el cuerpo
    const type = req.body.type;

    let permission = new Permission({
        description: description,
        type: type
    });

    acl.allow(description,  type);

    permission.save().then(obj => res.status(200).json({
        //message: "Permiso creado correctamente.",
        "message": res.__('create.permisos'),
        obj: obj
    })).catch(ex => res.status(500).json({
        //message: "No se pudo almacenar el permiso.",
        "message": res.__('create.nopermisos'),
        obj: ex
    }));

}

function replace(req, res, next) {
    const id = req.params.id;
    let description = req.body.description ? req.body.description : "";
    let type = req.body.type ? req.body.type : "";

    let permission = new Object({
        _description: description,
        _type: type
    });

    acl.allow(description, type);

    Permission.findOneAndUpdate({"_id":id}, permission).then(obj => res.status(200).json({
        //message: `Permiso reemplazado con ID: ${id}`,
        "message": res.__('replace.permisos'),
        obj: obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo reemplazar la informacion del permiso con ID: ${id}`,
        "message": res.__('replace.nopermisos'),
        obj: ex
    }));
}

async function edit(req, res, next) {
    const id = req.params.id;
    let description = req.body.description;
    let type = req.body.type;
 

    const permission = await Permission.findOne({"_id":id});

    if(description) {
        permission._description = description;
    }
    
    if(type) {
        permission._type = type;
    }

    acl.allow(description, type);

    permission.save().then(obj => res.status(200).json({
        //message: `Permiso editado con ID: ${id}`,
        "message": res.__('edit.permisos'),
        obj: obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo editar la informacion del permiso con ID: ${id}`,
        "message": res.__('edit.nopermisos'),
        obj: ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Permission.remove({"_id":id}).then(obj => res.status(200).json({
        //message: `Permiso eliminado con ID: ${id}`,
        "message": res.__('destroy.permisos'),
        obj: obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo eliminar el permiso con ID: ${id}`,
        "message": res.__('destroy.nopermisos'),
        obj: ex
    }));
}

module.exports = {
    list, index, create, replace, edit, destroy
}
