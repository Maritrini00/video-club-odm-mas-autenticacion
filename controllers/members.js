const express =require('express');
const Member =require('../models/member');

function list(req, res, next) {
  Member.find().then(objs => res.status(200).json({
       //message: "Lista de miembros del sistema",
       "message": res.__('list.members'),
       obj: objs
   })).catch(ex => res.status(500).json({
       //message: "No se pudo consultar la info de los miembros",
       "message": res.__('list.nomembers'),
       obj: ex
   }));
}

function index(req,res,next){   
   const id =req.params.id;
   Member.findOne({"_id":id}).then(obj => res.status(200).json({
       //message: `Miembro almacenado con ID ${id}`,
       "message": res.__('index.members'),
       obj: obj
   })).catch(ex => res.status(500).json({
        //message: `No se pudo consultar la info del miembro con id ${id}`,
        "message": res.__('index.nomembers'),
        obj: ex
    }));
}

function create(req,res,next){
    const name = req.body.name;//implicitos o sobre el cuerpo
    const lastName = req.body.lastName;
    const phone = req.body.phone;
    const status = req.body.status;

    const city = req.body.city;
    const country = req.body.country;
    const number = req.body.number;
    const state = req.body.state;
    const street = req.body.street;

    const address = new Object({
        _city: city,
        _country: country,
        _number: number,
        _state: state,
        _street: street
    })

   
    let member = new Member({
        name:name,
        lastName:lastName,
        phone:phone,
        status:status,
        address:address
    });

    member.save().then(obj => res.status(200).json({
        //message : 'miembro creado correctamente',
        "message": res.__('create.members'),
        obj: obj
    })).catch(ex =>res.status(500).json({
        //message: 'No se pudo almacena al miembro',
        "message": res.__('create.nomembers'),
        obj: ex
    }));
}

function replace(req,res,next){
    const id = req.params.id;
    let name = req.body.name ? req.body.name :"";
    let lastName = req.body.lastName ? req.body.lastName:"";
    let phone = req.body.phone ? req.body.phone:"";
    let status = req.body.status ? req.body.status:"";

    let city = req.body.city ? req.body.city : "";
    let country = req.body.country ? req.body.country : "";
    let number = req.body.number ? req.body.number : "";
    let state = req.body.state ? req.body.state : "";
    let street = req.body.street ? req.body.street : "";

    let address = new Object({
        _city:city,
        _country:country,
        _number:number,
        _state:state,
        _street:street
    })


    let member = new Object({
        _name:name,
        _lastName:lastName,
        _phone:phone,
        _status:status,
        _address:address
    });

    Member.findOneAndUpdate({"_id":id},member).then(obj => res.status(200).json({
        //message: "Miembro remplazado correctamente",
        "message": res.__('replace.members'),
        obj:obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo remplazar el miembro con id ${id}`,
        "message": res.__('replace.nomembers'),
        obj: ex
    }));

}

function edit(req,res,next){
    const id = req.params.id;

    const name = req.body.name;//implicitos o sobre el cuerpo
    const lastName = req.body.lastName;
    const phone = req.body.phone;
    const status = req.body.status;
    const city = req.body.city;
    const country = req.body.country;
    const number = req.body.number;
    const state = req.body.state;
    const street = req.body.street;

    let member = new Object();

    if(name){
        member._name = name;
    }

    if(lastName){
        member._lastName = lastName;
    }


    if(phone){
        member._phone = phone;
    }
    
    if(status){
        member._status = status;
    }

    if(city){
        member._address.set('_city', city);
    }

    if(country){
        member._address.set('_country', country);
    }

    if(number){
        member._address.set('_number', number);
    }

    if(state){
        member._address.set('_state', state);
    }

    if(street){
        member._address.set('_street', street);
    }


    Member.findOneAndUpdate({"_id":id},member).then(obj => res.status(200).json({
        //message: "miembro Actualizado correctamente",
        "message": res.__('edit.members'),
        obj:obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo actualizar el miembro con id ${id}`,
        "message": res.__('edit.nomembers'),
        obj: ex
    }));
}

function destroy(req,res,next){
    const id = req.params.id;
    Member.remove({"_id":id}).then(obj => res.status(200).json({
        //message: "Miembro eliminado correctamente",
        "message": res.__('destroy.members'),
        obj:obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo eliminar el miembro con id ${id}`,
        "message": res.__('destroy.nomembers'),
        obj: ex
    }));
}

module.exports ={
    list, index,create,edit,replace,destroy
}