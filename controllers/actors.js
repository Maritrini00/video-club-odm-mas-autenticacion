const express =require('express');
const Actor =require('../models/actor');

function list(req, res, next) {
let page = req.params.page ? req.params.page : 1;
   Actor.paginate({},{page:page,limit:3}).then(objs => res.status(200).json({
       "message": res.__('list.actores'),
       obj: objs
   })).catch(ex => res.status(500).json({
        "message": res.__('list.noactores'),
       obj: ex
   }));
}

function index(req,res,next){   
   const id =req.params.id;
   Actor.findOne({"_id":id}).then(obj => res.status(200).json({
       //message: `Actor almacenado con ID ${id}`,
       "message": res.__('index.actores'),
       obj: obj
   })).catch(ex => res.status(500).json({
        //message: `No se pudo consultar la info del actor con id ${id}`,
        "message": res.__('index.noactores'),
        obj: ex
    }));
}

function create(req,res,next){
    const name = req.body.name;//implicitos o sobre el cuerpo
    const lastName = req.body.lastName;
   
    let actor = new Actor({
        name: name,
        lastName:lastName
    });

    actor.save().then(obj => res.status(200).json({
        //message : 'Actor creado correctamente',
        "message": res.__('create.actores'),
        obj: obj
    })).catch(ex =>res.status(500).json({
        //message: 'No se pudo almacena el actor',
        "message": res.__('create.noactores'),
        obj: ex
    }));
}

function replace(req,res,next){
    const id = req.params.id;
    let name = req.body.name ? req.body.name: "";
    let lastName = req.body.lastName ? req.body.lastName: "";

    let actor = new Object({
        _name: name,
        _lastName : lastName
    });

    Actor.findOneAndUpdate({"_id":id},actor).then(obj => res.status(200).json({
        //message: "Actor remplazado correctamente",
        "message": res.__('replace.actores'),
        obj:obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo remplazar el actor con id ${id}`,
        "message": res.__('replace.noactores'),
        obj: ex
    }));

}

function edit(req,res,next){
    const id = req.params.id;
    let name = req.body.name;
    let lastName = req.body.lastName;

    let actor = new Object();

    if(name){
        actor._name = name;
    }

    if(lastName){
        actor._lastName = lastName;
    }

    Actor.findOneAndUpdate({"_id":id},actor).then(obj => res.status(200).json({
        //message: "Actor Actualizado correctamente",
        "message": res.__('edit.actores'),
        obj:obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo actualizar el actor con id ${id}`,
        "message": res.__('edit.noactores'),
        obj: ex
    }));
}

function destroy(req,res,next){
    const id = req.params.id;
    Actor.remove({"_id":id}).then(obj => res.status(200).json({
        //message: "Actor eliminado correctamente",
        "message": res.__('destroy.actores'),
        obj:obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo eliminar el actor con id ${id}`,
        "message": res.__('destroy.noactores'),
        obj: ex
    }));
}

module.exports ={
    list, index,create,edit,replace,destroy
}