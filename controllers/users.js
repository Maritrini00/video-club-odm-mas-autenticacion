const express =require('express');
const bcrypt = require('bcrypt');
const async = require ('async');
const acl = require('acl');
const config = require('config');
const User = require ('../models/user');


function list(req, res, next) {
    let page = req.params.page ? req.params.page : 1;
    User.paginate({},{page:page,limit:config.get("paginate.size")}).then(objs => res.status(200).json({
        //message: "Lista de usuarios del sistema",
        "message": res.__('list.users'),
        obj: objs
    })).catch(ex => res.status(500).json({
        //message: "No se pudo consultar la info de los usuarios",
        "message": res.__('list.nousers'),
        obj: ex
    }));
}

function index(req,res,next){   
    const id =req.params.id;
   User.findOne({"_id":id}).then(obj => res.status(200).json({
       //message: `usuario almacenado con ID ${id}`,
       "message": res.__('index.users'),
       obj: obj
   })).catch(ex => res.status(500).json({
        //message: `No se pudo consultar la info del usuario con id ${id}`,
        "message": res.__('index.users'),
        obj: ex
    }));
}

function create(req,res,next){
    const email = req.body.email;
    const name = req.body.name;//implicitos o sobre el cuerpo
    const lastName = req.body.lastName;
    const password =req.body.password;

    async.parallel({
        salt: (callback)=>{
            bcrypt.genSalt(10,callback);
        }
    },(err,result)=>{
        bcrypt.hash(password, result.salt,(err,hash)=>{
            let user = new User({
                email:email,
                name: name,
                lastName: lastName,
                password: hash,
                salt: result.salt
            });
            user.save().then(obj => res.status(200).json({
                //message : 'user creado correctamente',
                "message": res.__('create.users'),
                obj: obj
            })).catch(ex =>res.status(500).json({
                //message: 'No se pudo almacena el user',
                "message": res.__('create.users'),
                obj: ex
            }));
        });
    });
     
}

function replace(req,res,next){
    const id = req.params.id;
    let email = req.body.email ? req.body.email: "";
    let name = req.body.name ? req.body.name: "";
    let lastName = req.body.lastName ? req.body.lastName: "";
    let password = req.body.password ? req.body.password: "";
    let permissions = req.body.permissions ? req.body.permissions : "";

    let user = new Object({
        _email: email,
        _name: name,
        _lastName : lastName,
        _password : password,
        _permissions: permissions
    });

    User.findOneAndUpdate({"_id":id},user).then(obj => res.status(200).json({
        //message: "user remplazado correctamente",
        "message": res.__('replace.users'),
        obj:obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo remplazar el user con id ${id}`,
        "message": res.__('replace.users'),
        obj: ex
    }));
}

function edit(req,res,next){
    const id = req.params.id;
    let email = req.body.email;
    let name = req.body.name;
    let lastName = req.body.lastName;
    let password = req.body.password;
    let permissions = req.body.permissions;

    let user = new Object();

    if(email){
        user._email = email;
    }

    if(name){
        user._name = name;
    }

    if(lastName){
        user._lastName = lastName;
    }

    if(password){
        user._password =password;
    }
    
    if(permissions) {
        user._permissions = permissions;
      }    

   User.findOneAndUpdate({"_id":id},user).then(obj => res.status(200).json({
        //message: "user Actualizado correctamente",
        "message": res.__('edit.users'),
        obj:obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo actualizar el user con id ${id}`,
        "message": res.__('edit.users'),
        obj: ex
    }));
}

function destroy(req,res,next){
    const id = req.params.id;
    User.remove({"_id":id}).then(obj => res.status(200).json({
        //message: "User eliminado correctamente",
        "message": res.__('destroy.users'),
        obj:obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo eliminar el user con id ${id}`,
        "message": res.__('destroy.users'),
        obj: ex
    }));
}

module.exports ={
    list, index,create,edit,replace,destroy
}