const express =require('express');
const Movie =require('../models/movie');

function list(req, res, next) {
    let page = req.params.page ? req.params.page : 1;
    Movie.paginate({},{page:page, limit:3}).then(objs => res.status(200).json({
       //message: "Lista de peliculas del sistema",
       "message": res.__('list.movies'),
       obj: objs
    })).catch(ex => res.status(500).json({
       //message: "No se pudo consultar la info de las peliculas",
       "message": res.__('list.nomovies'),
       obj: ex
    }));
}

function index(req,res,next){   
   const id =req.params.id;
   Movie.findOne({"_id":id}).then(obj => res.status(200).json({
       //message: `Pelicula almacenada con ID ${id}`,
       "message": res.__('index.movies'),
       obj: obj
   })).catch(ex => res.status(500).json({
        //message: `No se pudo consultar la info de la pelicula con id ${id}`,
        "message": res.__('index.nomovies'),
        obj: ex
    }));
}

function create(req,res,next){
    const genre = req.body.genre;//implicitos o sobre el cuerpo
    const title = req.body.title;
    const actors = req.body.actors;
    const directorName = req.body.directorName;
    const directorLastName = req.body.directorLastName;
    
    const director = new Map([
        ['name', directorName],
        ['lastName', directorLastName],
    ]);
   
    let movie = new Movie({
        genre:genre,
        tilte:title,
        director:director,
        actors:actors
    });

    movie.save().then(obj => res.status(200).json({
        //message : 'pelicula creada correctamente',
        "message": res.__('create.movies'),
        obj: obj
    })).catch(ex =>res.status(500).json({
        //message: 'No se pudo almacena la pelicula',
        "message": res.__('create.nomovies'),
        obj: ex
    }));
}

function replace(req,res,next){
    const id = req.params.id;
    const genre = req.body.genre ? req.body.genre: "";
    const title = req.body.title ? req.body.title: "";
    const actors = req.body.actors ? req.body.actors: "";
    const directorName = req.body.directorName ? req.body.directorName: "";
    const directorLastName = req.body.directorLastName ? req.body.directorLastName: "";

    const director = new Map([
        ['name', directorName],
        ['lastName', directorLastName],
    ]);

    let movie = new Object({
        _genre: genre,
        _title: title,
        _actors: actors,
        _director: director
    });

    Movie.findOneAndUpdate({"_id":id},movie).then(obj => res.status(200).json({
        //message: "pelicula remplazado correctamente",
        "message": res.__('replace.movies'),
        obj:obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo remplazar la pelicula con id ${id}`,
        "message": res.__('replace.nomovies'),
        obj: ex
    }));

}

function edit(req,res,next){
    const id = req.params.id;
    const genre = req.body.genre;//implicitos o sobre el cuerpo
    const title = req.body.title;
    const actors = req.body.actors;
    const directorName = req.body.directorName;
    const directorLastName = req.body.directorLastName;

    let movie = new Object();

    if(genre){
        movie._genre = genre;
    }
    
    if(title){
        movie._title = title;
    }

    if(actors){
        movie._actors = actors;
    }

    if(directorName){
        movie._director.set('_Name', directorName);
    }

    if(directorLastName){
        movie._director.set('_lastName', directorLastName);
    }

    Movie.findOneAndUpdate({"_id":id},movie).then(obj => res.status(200).json({
        //message: "pelicula Actualizada correctamente",
        "message": res.__('edit.movies'),
        obj:obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo actualizar la pelicula con id ${id}`,
        "message": res.__('edit.nomovies'),
        obj: ex
    }));
}

function destroy(req,res,next){
    const id = req.params.id;
    Movie.remove({"_id":id}).then(obj => res.status(200).json({
        //message: "Pelicula eliminado correctamente",
        "message": res.__('destroy.movies'),
        obj:obj
    })).catch(ex => res.status(500).json({
        //message: `No se pudo eliminar la pelicula con id ${id}`,
        "message": res.__('destroy.nomovies'),
        obj: ex
    }));
}

module.exports ={
    list, index,create,edit,replace,destroy
}