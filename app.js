const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const expressJwt = require('express-jwt');
const config = require ('config');
const i18n = require('i18n');

var acl = require('acl');

const jwtKey = config.get("secret.key");
console.log(jwtKey);

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const actorsRouter = require('./routes/actors');
//const directorsRouter = require('./routes/directors');
const membersRouter = require('./routes/members');
const bookingsRouter = require('./routes/bookings');
const copiesRouter = require('./routes/copies');
//const addressesRouter = require('./routes/addresses');
const moviesRouter = require('./routes/movies');
const permissionsRouter = require('./routes/permissions');



//"mongodb://<dbuser>?:<dbPassword>?@<direction>:<port>/<dbName>"

const uri = config.get("dbChain");

mongoose.connect(uri);

const db = mongoose.connection;
const app = express();

db.on('error',()=>{
  console.log("No se puele conectar a la base de datos :(");
  
});

db.on('open',()=>{
  console.log("conexion correcta");
  acl = new acl(new acl.mongodbBackend(db, "acl_"));


    acl.allow("Miembro", "/index", "view");


    acl.allow("administrador", "/", "*");
});

i18n.configure({
  locales : ['es','en'],
  cookie :'language',
  directory : `${__dirname}/locales`
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
//middleware static => define donde se encuentran todos los recursos estaticos de la app
app.use(express.static(path.join(__dirname, 'public')));
app.use(i18n.init);

app.use(expressJwt({secret:jwtKey,algorithms:['HS256']})
.unless({path:["/login"]}));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/actors', actorsRouter);
app.use('/copies', copiesRouter);
app.use('/members', membersRouter);
app.use('/bookings', bookingsRouter);
app.use('/movies', moviesRouter);
app.use('/permissions', permissionsRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
