const supertest = require('supertest');

const app = require ('../app');

var key = " ";

//sentencia
describe('probar el sistema de autenticación',()=>{
    //casos de prueba => 50%
    it('debería de obtener un login con usuario y contraseña correcto',(done)=>{
        supertest(app).post('/login')
        .send({'email':'a329675@uach.mx', 'password': 'almendra22'})
        .expect(200)
        .end(function(err,res){
            key = res.body.obj;
            done();
        });
    });
});

describe('probar las rutas de las copias', ()=>{
    it('debería de obtener la lista de copias',(done)=>{
        supertest(app).get('/copies/')
        .set('Authorization',`Bearer ${key}`)
        .end(function(err,res){
            if(err){
                done(err);
            }else{
                expect(res.statusCode).toEqual(200);
                done();
            }          
        });
    });
});