const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _title:String,
    _genre:String,
    _director : {type:Map, of:String},
    _actors : [{type: mongoose.Schema.ObjectId, ref: 'Actor'}]
});

class Movie {

    constructor(title,genre,director,actors){
        this._title = title;
        this._genre = genre;
        this._director = director;
        this._actors = actors;
    }

    get title(){
        return this._title;
    }

    set title(value){
        this._title = value;
    }

    get genre(){
        return this._genre;
    }

    set genre(value){
        this._genre = value;
    }

    get director(){
        return this._director;
    }

    set director(value){
        this._director = value;
    }

    get actors(){
        return this._actors;
    }

    set actors(value){
        this._actors = value;
    }

}

schema.loadClass(Movie);
module.exports = mongoose.model('Movie',schema);