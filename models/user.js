const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = mongoose.Schema({
    _email:String,
    _name:String,
    _lastName:String,
    _password:String,
    _salt:String,
    _permissions : [{type: mongoose.Schema.ObjectId, ref: 'Permission'}]
});

class User {

    constructor(email,name,lastName,password,salt,permissions){
        this._email = email;
        this._name = name;
        this._lastName = lastName;
        this._password = password;
        this._salt = salt;
        this._permissions = permissions;
    }

    get email(){
        return this._email;
    }

    set name(value){
        this._email = value;
    }
    get name(){
        return this._name;
    }

    set name(value){
        this._name = value;
    }

    get lastName(){
        return this._lastName;
    }

    set lastName(value){
        this._lastName = value;
    }
    get password(){
        return this._password;
    }

    set password(value){
        this._password = value;
    }
    get salt(){
        return this._salt;
    }

    set salt(value){
        this._salt = value;
    }

    get permissions(){
        return this._permissions;
    }

    set permissions(value){
        this._permissions = value;
    }

}

schema.loadClass(User);
schema.plugin(mongoosePaginate);
module.exports = mongoose.model('User',schema);