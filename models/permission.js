const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = mongoose.Schema({
    _description : String,
    _type: String
});

class Permission {

    constructor(description,type){
        this._description = description;
        this._type = type;
        
    }

    get description(){
        return this._description;
    }

    set description(value){
        this._description = value;
    }
    get type(){
        return this._type;
    }

    set type(value){
        this._type = type;
    }

}

schema.loadClass(Permission);
schema.plugin(mongoosePaginate);
module.exports = mongoose.model('Permission',schema);